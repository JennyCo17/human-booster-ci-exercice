Exercices Git, Gitlab et CI

1. Créer un fork de ce projet 
    - [x] cliquer sur le bouton "fork" en haut de l'écran de Gitlab
    - [x] Inviter vos binômes et @Dreeckan dans le projet, avec les droits d'écriture

2. Cloner ce projet sur vos machines (idéalement, en ligne de commande ;) )

3. Ajouter un fichier AUTHORS.md
    - [x] Créer une branche et une merge request sur Gitlab
    - [x] Récupérer cette branche en local
    - [x] Ajouter le fichier et son contenu en local
        - [x] Chaque personne du groupe devra inscrire son nom (un exemple de format que vous n'êtes pas obligés de suivre : `Rémi Jarjat <remi@drakona.fr>`) et faire un commit
    - [x] Mettre à jour la merge request et la relire à deux (vérifier que le contenu du fichier correspond aux attentes et que les 2 commits sont bien présents)
    - [x] Merge dans votre projet une fois les corrections validées

4. Lancer les tests unitaires (`./bin/phpunit` dans la ligne de commande)
    - [x] Créer une branche et une merge request sur Gitlab
    - [x] Récupérer cette branche en local
    - [x] Corriger les tests pour qu'ils ne renvoient plus une erreur et soient cohérents
    - [x] Mettre à jour la MR et la faire relire (ou la relire à deux)
    - [x] Merge dans votre projet une fois les corrections validées

5. Lancer phpStan
    - [x] Créer une branche et une merge request sur Gitlab
    - [x] Récupérer cette branche en local
    - [x] Corriger le code pour ne plus avoir d'erreur phpStan
    - [x] Mettre à jour la MR et la faire relire (ou la relire à deux)
    - [x] Merge dans votre projet une fois les corrections validées

6. Créer un fichier de CI gitlab ([Une aide sur Elevent Labs](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/) ou [un exemple de fichier gitlab](https://gitlab.com/snippets/1994308))
    - [x] Créer une branche et une merge request sur Gitlab
    - [x] Récupérer cette branche en local
    - [x] Créer le fichier de CI (.gitlab-ci.yml)
        - [x] Ajouter 3 tâches (jobs) : 
            - [x] Installation des vendors de Symfony et mise en cache de ce dossier
            - [x] Lancement des tests unitaires
            - [x] Lancement de phpStan et rendre cette tâche optionnelle (si elle renvoie une erreur, elle ne doit pas arrêter le pipeline)
        - [x] Répartir ces tâches dans au moins 2 étapes (stages)
        - [x] Utiliser une image Docker pour lancer le pipeline (pour ceux qui préfèrent ne pas chercher, vous pouvez utiliser celle-ci : `drakona/php:7.2-ci`)
    - [x] Mettre à jour la MR et la faire relire (ou la relire à deux)
    - [x] Merge dans votre projet une fois les corrections validées